let url = 'https://www.liberty.edu/online/'
let controller = new (require('../page-controllers/learn-more.controller'))

let values = {
  level: 'Undergraduate',
  degree: 'ACCT-AA-D',
  fname: !!process.env.npm_package_config_fname ? process.env.npm_package_config_fname : 'NO NAME',
  lname: 'tests',
  email: 'test@liberty.edu',
  phone: '555-555-5555',
  country: 'US',
  street: '123 main st',
  zip: '12345',
  city: 'testville',
  state: 'Virginia'
}

describe('LUO Leads Form', function() {

  beforeAll(() => {
    browser.get(url);
    browser.sleep(500)
  })

  controller.submitWithValues(values)

});
