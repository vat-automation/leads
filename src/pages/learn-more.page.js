module.exports = function() {

    let learnMorePage = this

    //variables

    learnMorePage.openForm = () => {
        return element(by.cssContainingText('a.btn.dark.request-info.request-ug', 'Undergraduate')).click()
    }

    learnMorePage.selectLevel = (level) => { 
        return element(by.cssContainingText('label.radio', level)).click()
    }

    learnMorePage.selectDegree = (degree) => { 
        return element(by.css(`#degree-study > option[value=${degree}]`)).click()
    }

    learnMorePage.enterFirstName = (fname) => { 
        return element(by.css('#lead > div:nth-child(6) > label:nth-child(1) > input')).sendKeys(fname)
    }

    learnMorePage.enterLastName = (lname) => { 
        return element(by.css('#lead > div:nth-child(6) > label:nth-child(2) > input')).sendKeys(lname)
    }
    
    learnMorePage.enterEmail = (email) => { 
        return element(by.css('#lead > div:nth-child(7) > label:nth-child(1) > input')).sendKeys(email)
    }

    learnMorePage.enterPhone = (number) => { 
        return element(by.css('#lead > div:nth-child(7) > label:nth-child(2) > input')).sendKeys(number)
    }

    learnMorePage.selectCountry = (code) => { 
        return element(by.css(`#country > option[value=${code}]`)).click()
    }

    learnMorePage.enterStreet = (street) => { 
        return element(by.id('address1')).sendKeys(street)
    }

    learnMorePage.enterZip = (zip) => { 
        return element(by.id('zip')).sendKeys(zip)
    }

    learnMorePage.enterCity = (city) => { 
        return element(by.id('city')).sendKeys(city)
    }

    learnMorePage.enterState = (state) => { 
        return element(by.id('state')).sendKeys(state)
    }

    learnMorePage.submit = () => { 
        return element(by.id('homepageSubmit')).click()
    }







}
