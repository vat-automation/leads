let learnMorePage = new (require("../pages/learn-more.page.js"))


module.exports = function(){
    let learnMoreController = this

    //variables

    learnMoreController.submitWithValues = (values) => describe('Submitting form with values: ' + values, () => {
        it('Form Submitted', () => {
            expect(learnMorePage.openForm())
            browser.sleep(2500)
            //expect(learnMorePage.selectLevel(values.level))
            expect(learnMorePage.selectDegree(values.degree))
            expect(learnMorePage.enterFirstName(values.fname))
            expect(learnMorePage.enterLastName(values.lname))
            expect(learnMorePage.enterEmail(values.email))
            expect(learnMorePage.enterPhone(values.phone))
            expect(learnMorePage.selectCountry(values.country))
            expect(learnMorePage.enterStreet(values.street))
            expect(learnMorePage.enterZip(values.zip))
            expect(learnMorePage.enterCity(values.city))
            expect(learnMorePage.enterState(values.state))
            expect(learnMorePage.submit())
            browser.sleep(2500)
            expect(browser.getCurrentUrl()).toMatch('https://www.liberty.edu/online/request-information-thank-you/*')
        })
    })

    
}