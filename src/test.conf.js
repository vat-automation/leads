var JSONReporter = require('jasmine-bamboo-reporter');
var fs = require('fs');

/**
 * I copied some of this stuff from soc-student-ui proctor.conf.js.
 */

 exports.config = {
  framework: 'jasmine2',

  beforeLaunch: function () {
    //clean up any residual/leftover from a previous run. Ensure we have clean
    //files for both locking and merging.
    if (fs.existsSync('jasmine-results.json.lock')) {
      fs.unlinkSync('jasmine-results.json.lock');
    }
    if (fs.existsSync('jasmine-results.json')) {
      fs.unlink('jasmine-results.json');
    }
},

  onPrepare: function () {
    browser.driver.manage().window().maximize()
    browser.ignoreSynchronization = true

    jasmine.getEnv().addReporter(new JSONReporter({
      file: 'jasmine-results.json', // by default it writes to jasmine.json
      beautify: true,
      indentationLevel: 4 // used if beautify === true
    }));
  },

  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['tests/*.spec.js'],

  
  capabilities: {
    'browserName':'chrome',
    "chromeOptions": { "args": [ "--headless", "--disable-gpu", "--window-size=800,600", "--no-sandbox", "--disable-dev-shm-usage"] }
  },

  jasmineNodeOpts: {
    onComplete: null,
    isVerbose: false,
    showColors: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 60000,

    //This empty print function stops the default protractor dot reporter from running
    //If this is erased then both will run
    print: function () { }
  },

  allScriptsTimeout: 200000
};
